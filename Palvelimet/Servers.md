Inventory listing
=================

Listaus osista, niiden määristä ja sarjanumeroista

  - Ylläpitäjä; Sami Nieminen

Versio
----

1.0

Palvelinten osat
----------------

Kovalevyt:

| Tila (GB) | Malli    | Mallinumero | Sarjanumero | Käytössä |
|-----------|----------|-------------|-------------|----------|
| 146       | 10K SAS  | ST9146802SS | 3NM14Y8F    | Kyllä    |
| 146       | 10K SAS  | ST9146802SS | 3NM15EAX    | Kyllä    |
| 146       | 10K SAS  | ST9146802SS | 3NM15FPN    | DOA      |
| 146       | 10K SAS  | ST9146802SS | 3NM1572V    | Kyllä    |
| 146       | 10K SAS  | ST9146802SS | 3NM2JZ5C    | Kyllä    |
| 146       | 10K SAS  | ST9146802SS | 3NM2PDD1    | DOA      |
| 146       | 10K SAS  | ST9146802SS | 3NM15EX0    | Kyllä    |
| 146       | 10K SAS  | ST9146802SS | 3NM2JZ2R    | Kyllä    |
| 36        | 15K SCSI | MAU3036NC   | PHJ537F0WF  | Kyllä    |
| 36        | 15K SCSI | MAU3036NC   | PHJ539FI5G  | Kyllä    |
| 36        | 15K SCSI | MAU3036NC   | PHJ539F16S  | Kyllä    |
| 36        | 15K SCSI | MAU3036NC   | PHJ539F15B  | Kyllä    |
| 36        | 15K SCSI | MAU3036NC   | PHJ537F0VW  | Kyllä    |
| 36        | 15K SCSI | BD03664553  | 3FD1LBBE    | Kyllä    |
| 146       | 15K SCSI | ST3146854LC | 3KN16RDT    | Kyllä    |
| 146       | 15K SCSI | ST3146854LC | 3KN16QWL    | Kyllä    |
| 146       | 15K SCSI | ST3146854LC | 3KN1495G    | Kyllä    |
| 36        | 15K SCSI | BF03685A35  | 3HX04LXE    | Kyllä    |
| 36        | 15K SCSI | BF03688575  | PHJ539F149  | Kyllä    |
| 36        | 15K SCSI | BF03688575  | PHJ539F155  | Kyllä    |

Fyysiset palvelimet
-------------------

| Nimi             | Muisti           | Prosessori            | Sarjanumero  | Käyttöjärjestelmä  | Käytössä |
|------------------|------------------|-----------------------|--------------|--------------------|----------|
| IBM-x3250m3-1234 | 32GB ECC 1333Mhz | 1 x 3.2Ghz Intel XEON | 968745061234 | xenserver-ALPHA-03 |  Kyllä   |
|                  |                  |                       |              |                    |          |
|                  |                  |                       |              |                    |          |