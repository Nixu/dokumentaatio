VhostManager pikaopas
=====================

Koko oppaan löydät englanniksi osoitteesta [Winded/Vhostmgr](https://bitbucket.org/Winded/vhostmgr)

### Kirjautuminen

1. Aloita menemällä osoitteeseen ```https://hylab.hopto.org:8899```
2. Kirjoita käyttäjätunnuksesi sekä salasanasi ja paina ```Kirjaudu sisään```
3. Nyt olet etusivulla

![alt text](http://i.imgur.com/elFup7r.png "Etusivu")

### Vuokraaminen

1. Paina etusivulta ```Vuokraukset```
2. Paina sivun vasemmasta laidasta ```Uusi vuokraus```
3. Voit valita vuokraamasi virtuaalipalvelimen ominaisuudet tästä. 

> ![alt text](http://i.imgur.com/wfkdgXa.png "Vuokraus")

> * Ensimmäiseksi valitaan virtuaalipalvelin. Voit valita minkä tahansa vapaan palvelimen.
> * Seuraavaksi valitaan aloituspäivämäärä. Aloitusajaksi kannattaa valita noin viisi minuuttia nykyisestä ajasta eteenpäin, jotta vuokraus toimii oikein.
> * Sitten lopetuspäivämäärä. Maksimiaika on 120 päivää, jonka jälkeen tiedostot poistuvat.
> * Ja viimeiseksi valitaan sivupaketti jonka haluat asentaa palvelimeesi. Olemme asentaneet valmiiksi jo aika kattavan valikoiman sivupaketteja. On myös mahdollista jättää tämä tyhjäksi jos haluat tehdä sivustosi itse.
> * Paina vielä ```vuokraa```

### Palvelinten käyttö

* Saat omat sivustosi näkymään kun menet osoitteeseen ```hylab.hopto.org:(vuokrapalvelimesi numero)```.
* Voit ladata tiedostoja ja siirtää niitä millä tahansa SFTP-protokollaa tukevalla tiedonsiirto ohjelmalla.
* SSH ja SFTP yhteyksiä varten on avattu portti numero ```22```. Tunnukset ovat samat kuin millä kirjauduit sivustoon.

### Lisä-info
* Lisäinfoa saa Sami Niemiseltä ```nieminen.sami2@gmail.com```