# Hyria laboratorioluokan dokumentaatio #

Tästä dokumentaatiosta löydät kaiken palvelinten konfiguraatiosta erilaisten palvelujen asentamiseen.

## Pikaohje Git:n käyttöön ##

1. Lataa Git [Windows](http://git-scm.com/download/win)
2. Asenna Git ohjeiden mukaisesti
3. Kloonaa tämä repo 

    `git config --global user.name "FIRST_NAME LAST_NAME"`

    `git config --global user.email "MY_NAME@example.com"`

    `git clone https://"käyttäjänimi@bitbucket.org"/hylabadmins/dokumentaatio.git`

4. Kun olet tehnyt tarvittavat muutokset, tarkista repon tila

    `git status`

5. Lisää muutetut tiedostot

    `git add .`

6. Lähetä tiedostot palvelimelle

    `git push -u origin master`

7. Jos jokin komento jäi askarruttamaan mieltä, tai haluat tietää lisää, käy katsomassa [Git Wiki](https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+Documentation+Home)