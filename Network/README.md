#Verkot
---
Verkot on tehty mahdollisimman helpoiksi jotta voit vain kytkeä laitteen oikeaan verkkoon ja se vain toimii.

**Xen palvelimet aina omaan verkkoonsa!** Palvelimiin pääsee asennuksen jälkeen käsiksi alla olevista osoitteista ``XenTools`` ohjelmistolla.

---

##Labraverkko

Labraverkossa on käytössä DHCP joka jakaa automaattisesti osoitteita väliltä

``192.168.1.40 - 192.168.1.150``

DHCP jakaa myös seuravaat tiedot

**NTP** = ``192.168.1.1``

**DNS** = ``192.168.1.1``

**Domain** = ``labra.hylab``

**Oletusyhdyskäytävä** = ``192.168.1.1``

---

##Osoitteet

| Kuvaus                  | Verkko/CIDR     | Yhdyskäytävä  | DHCP   |
|:------------------------|:----------------|:--------------|:------:|
| Labraverkko             | 192.168.1.0/24  | 192.168.1.1   |Kyllä   |
| Hallintaverkko          | 192.168.2.0/24  | 192.168.2.1   |Ei      |
| VPN                     | 192.168.3.0/24  | 192.168.3.1   |Kyllä   |
| WLAN                    | 192.168.4.0/24  | 192.168.4.1   |Kyllä   |
| Palvelimet              | 192.168.5.0/24  | 192.168.5.1   |Ei      |
| XenServers              | 192.168.50.0/24 | 192.168.50.1  |Kyllä   |


---

##Oppilaiden omat virtuaaliverkot

Nämä verkot on eristetty muusta liikenteestä jotta voidaan testata esimerkiksi DHCP-palvelimien asennusta.

Näissä verkoissa ei ole DHCP päällä koska se sekoittaisi omia asennuksia.

| Kuvaus               	  |	Verkko/CIDR     | Yhdyskäytävä	|
|:------------------------|:----------------|:--------------|
| Vlan 0                  | 192.168.20.0/24 | 192.168.20.1  |
| Vlan 1                  | 192.168.21.0/24 | 192.168.21.1  |
| Vlan 2                  | 192.168.22.0/24 | 192.168.22.1  |
| Vlan 3                  | 192.168.23.0/24 | 192.168.23.1  |
| Vlan 4                  | 192.168.24.0/24 | 192.168.24.1  |
| Vlan 5                  | 192.168.25.0/24 | 192.168.25.1  |
| Vlan 6                  | 192.168.26.0/24 | 192.168.26.1  |
| Vlan 7                  | 192.168.27.0/24 | 192.168.27.1  |
| Vlan 8                  | 192.168.28.0/24 | 192.168.28.1  |
| Vlan 9                  | 192.168.29.0/24 | 192.168.29.1  |
| Vlan 10                 | 192.168.30.0/24 | 192.168.30.1  |
| Vlan 11                 | 192.168.31.0/24 | 192.168.31.1  |


---

##XenServers

Xen palvelimille tarkoitetussa verkossa on käytössä DHCP asennuksen yksinkertaistamiseksi.

DHCP-palvelin jakaa osoitteita välillä.

``192.168.50.40 - 192.168.50.100``

DHCP jakaa myös tarvittavat NTP-palvelimet sekä oikean DNS ja oletusyhdyskäytävän.

**NTP** = ``192.168.1.1``

**DNS** = ``192.168.1.1``

**Domain** = ``xen.hylab``

**Oletusyhdyskäytävä** = ``192.168.50.1``

Xen palvelimille on erikseen varattu omat osoitteet PfSensestä jotta palvelimet ja virtuaalipalvelimet saavat eri osoitteet!

**Xen palvelimien varatut osoitteet**

| Nimi						| IP-osoite			|
|---------------------------|-------------------|
|xenserver-ALPHA-00			|192.168.50.10		|
|xenserver-ALPHA-01			|192.168.50.11		|
|xenserver-ALPHA-02			|192.168.50.12		|
|xenserver-ALPHA-03			|192.168.50.13		|
|xenserver-ALPHA-04			|192.168.50.14		|
|xenserver-ALPHA-05			|192.168.50.15		|

---

##iLO/IMM hallinnan osoitteet palvelimille.

* iLO = HP palvelimien etähallinta.
* IMM = IBM palvelimien etähallinta.

**HUOM** Kaikissa palvelimissa ei ole etähallintaa asennettuna!

| Palvelin                | IP-osoite       | Etähallinta   |
|-------------------------|-----------------|---------------|
| Tuotantopalvelin        | 192.168.5.3     | 192.168.2.50  |


---

##Portit

Tästä taulukosta löydät verkkoja vastaavat kytkinportit.

| Laite            | Hallintaosoite        | Malli                 |
|------------------|-----------------------|----------------------:|
| s00              | 192.168.2.5           | Cisco Catalyst 3550   |
| s01              | 192.168.2.2           | Cisco Catalyst 2950   |
| s02              | 192.168.2.3           | Cisco Catalyst 2900XL |
| s03              | 192.168.2.4           | Cisco Catalyst 2950   |


###S00 - Cisco Catalyst 3550

| Verkko           | Portit                | **HUOM**              |
|------------------|-----------------------|-----------------------|
| Perusverkko      | Gi0/1 – 2             | Kuitu - ei käytössä   |
| Hallintaverkko   | Fa0/13 – 20           |                       |
| Palvelimet       | Fa0/1 – 4             |                       |
| XenServer        | Fa0/5 - 12            |                       |
| Trunk-portit     | Fa0/21 – 24           | switchport mode trunk |


###S01 & S03 - Cisco Catalyst 2950

| Verkko           | Portit                | **HUOM**              |
|------------------|-----------------------|-----------------------|
| Perusverkko      | Gi0/1 – 2             | Kuitu - ei käytössä   |
| Vlan 0           | Fa0/1 – 4             |                       |
| Vlan 1           | Fa0/5 – 8             |                       |
| Vlan 2           | Fa0/9 – 12            |                       |
| Vlan 3           | Fa0/13 – 16           |                       |
| Vlan 4           | Fa0/17 – 20           |                       |
| Vlan 5           | Fa0/21 – 24           |                       |
| Vlan 6           | Fa0/25 – 28           |                       |
| Vlan 7           | Fa0/29 – 32           |                       |
| Vlan 8           | Fa0/33 – 36           |                       |
| Vlan 9           | Fa0/37 – 40           |                       |
| Vlan 10          | Fa0/41 – 44           |                       |
| Vlan 11          | Fa0/45 – 47           |                       |
| Trunk-portit     | Fa0/48                | switchport mode trunk |


###S02 - Cisco Catalyst 2900XL

| Verkko           | Portit                | **HUOM**              |
|------------------|-----------------------|-----------------------|
| Perusverkko      | Gi0/1 – 2             | Kuitu - ei käytössä   |
| Perusverkko      | Fa0/1 – 23            |                       |
| Trunk-portit     | Fa0/24                | switchport mode trunk |
