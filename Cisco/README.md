# Kytkinten konfiguraatiot #

Laboratorioluokan kytkinten konfiguraatiot.

Kytkinten IOS imaget ovat vielä TFTP palvelimella, tarkoituksena on ladata ne tänne, jos eivät ole liian suurikokoisia.

## Kuinka käyttää näitä tiedostoja ##

1. Kirjaudu kytkimelle sisään
2. Kirjoita konsoliin

    `enable`

3. Jonka jälkeen

    `configure terminal`

4. Tämän jälkeen sinun pitäisi pystyä kopioimaan kaikki komennot tiedoston sisältä konsoliin
5. Poistu konfiguraatio tilasta

    `end`

6. Tallenna muutokset muistiin jotta ne säilyvät vaikka laite käynnistettäisiin uudestaan

    `copy running-config startup-config`

7. Paina enter
8. Lisää komentoja; [Cisco IOS command sheet](http://www.cisco.com/c/en/us/support/ios-nx-os-software/ios-15-1m-t/products-installation-and-configuration-guides-list.html)

	