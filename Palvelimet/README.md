# Palvelimet


Tässä dokumentissa on nimeämiskäytännöt sekä palvelinten ja niiden osien sarjanumerot.

Mukana myös ohjeet kuinka palvelimet nimetään.

---

## Tiedostot

1. ``Inventory.md`` sisältää kaikki palvelinten osien sarjanumerot.
2. ``Servers.md`` sisältää palvelimet ja niiden konfiguraatiot ja mitä niissä on tällä hetkellä asennettuna.
3. ``Readme.md`` on tämä tiedosto.

---

## Nimeämiskäytäntö käyttöjärjestelmille

* Ensimmäisenä on palvelimen käyttötarkoitus, esim ``xenserver``. **Kaikki pienellä!**
* Toisena on versio, esim xenserverin alpha julkaisuversio ``ALPHA`` **Kaikki isolla!**
* Kolmantena tulee vielä monesko palvelin se on, esim ``03`` **Kaksoisnumerolla!**
* Lisää vielä väleihin viivat.
* Lopputuloksena ``xenserver-ALPHA-03``. Tästä tiedämme että palvelimella on kolmas xenserver jonka versio on alpha.
* Lisää tämä ``Servers.md`` tiedostoon jos palvelimesta on jo tehty fyysinen kartoitus

**Tärkeää on että tätä noudatetaan, muuten tulee sekaannusta!**

---

## Fyysinen nimeämiskäytäntö

Jotta tiedämme millä koneella mikäkin käyttöjärjestelmä on, pitää dokumentaatiossa käydä aina ilmi mitä koneelle on asennettu!

* Ensimmäisenä palvelimen valmistaja, esim ``IBM``
* Toisena malli, esim ``x3250m3``
* Kolmantena vielä sarjanumeron viimeiset 4 numeroa, esim ``1234``
* Lisää vielä väleihin viivat.
* Lopputulos ``IBM-x3250m3-1234``
* Jos on käytössä tarrakone, tulosta se ja liimaa se palvelimen **etu- ja takaosaan!**
* Lopuksi vielä lisää se dokumentaatioon tiedostoon ``Servers.md`` ja laita mukaan myös onko koneeseen jo valmiiksi asennettu jotakin

---

## Kuinka lisään palvelimen?

1. Kopioi yksi tyhjä rivi tiedostosta.
2. Lisää tarvittavat tiedot mahdollisimman tarkasti ja nimeämiskäytäntöä noudattaen

---

## Työkaluja editoimiseen


[Dillinger.io](http://www.dillinger.io) **Todella kätevä live markdown editori**

[Markdown tables generator](http://www.tablesgenerator.com/markdown_tables) **Kätevä työkalu taulukointiin**

---